'use strict'

const agent = {
    id: 1,
    uuid: 'yyy-yyy-yyy',
    name: 'fixture',
    username: 'cometacos',
    hostname: 'test-host',
    pid: 0,
    connected: true,
    createdAt: new Date(),
    updatedAt: new Date()
}

const agents = [
    agent,
    extend(agent, {
        id: 2,
        uuid: 'yyy-yyy-yyw',
        username: 'test',
        connected: true,
    }),
    extend(agent, {
        id: 3,
        uuid: 'yyy-yyy-yyx',
        username: 'testx',
        connected: false,
    })

]

function extend(obj, values) {
    const clone = Object.assign({}, obj)
    return Object.assign(clone, values)
}

module.exports = {
    single: agent,
    all: agents,
    connected: agents.filter(a => a.connected),
    cometacos: agents.filter(a => a.username === 'cometacos'),
    byUuid: id => agents.filter(a => a.uuid === id).shift(),
    byId: id => agents.filter(a => a.id === id).shift()
}