# spiderverse-db

## Usage

``` js
const setupDatabase = require('spiderverse-db')

setupDatabase(config).then(db => {
    const { Agent, Metric } = db
}).catch(err => console.error(err))
```
